/* eslint-disable camelcase */
import colorList from './static/color-list.json'
import fontList from './static/fonts/font-list.json'
import imageList from './static/images/image-list'

const formState = {}

const FONT_SIZE_INITIAL = 4
const FONT_SIZE_MAX = 12
const FONT_SIZE_MIN = 0.5
const CONTACT_API = 'https://formspree.io/f/xyybjnkv'
const LIGHT_SUBMISSION_FORM = 'light-submission-form'
const USER_TEXT = 'message'
const WALL_WRITING = 'wall-writing'
const USER_TEXT_SCALE = 'user-text-scale'
const USER_TEXT_Y_AXIS = 'user-text-y-axis'
const USER_TEXT_X_AXIS = 'user-text-x-axis'
const COLOR_LIST = 'color-list'
const FONT_LIST = 'font-list'
const IMAGE_LIST = 'image-list'
const CSS_PROPERTY_TEXT_SHADOW = 'text-shadow'
const CSS_PROPERTY_FONT_FAMILY = 'font-family'
const CSS_PROPERTY_BOX_SHADOW = 'box-shadow'
const SELECTED_IMAGE = 'selected-image'
const NEON_BACKGROUND_IMAGE = 'neon-background-image'
const LOADER = 'loader'
const COMPLETED_ENQUIRY = 'completed-enquiry'
const ERROR_ENQUIRY = 'error-enquiry'
const FORM_STATE = 'form-state'

const getElement = elementName =>
	document.querySelector(`[data-id=${elementName}]`)
const graph = (value, {min, max}) => ((max - min) / 100) * value + min
const makeShadow = color =>
	'0 0 5px #fff, 0 0 10px #fff, 0 0 20px ' +
  color +
  ', 0 0 30px ' +
  color +
  ', 0 0 40px ' +
  color +
  ', 0 0 55px ' +
  color +
  ', 0 0 75px ' +
  color

const setFontSize = rem => {
	elementWriting.style['line-height'] = `${rem}rem`
	elementWriting.style['font-size'] = `${rem}rem`
}

const updateFormState = object => {
	if (object.name && object.dataset && object.dataset.value) {
		// Important field
		formState[object.name] = object.dataset.value
		formState[`Additional Property:\n  ${object.name}`] = object.value
		return
	}

	if (object.name) {
		// Important field
		formState[object.name] = object.value
		return
	}

	if (object.dataset && object.dataset.id) {
		// Store param to reproduce env from link
		formState[object.dataset.id] = object.value
	}
}

const stringMessageFromObject = object =>
	Object.keys(object)
		.map(curr => `${curr}: ${object[curr]}\n`)
		.join('')

const elementForm = getElement(LIGHT_SUBMISSION_FORM)
const elementInput = getElement(USER_TEXT)
const elementWriting = getElement(WALL_WRITING)
const elementScale = getElement(USER_TEXT_SCALE)
const elementTop = getElement(USER_TEXT_Y_AXIS)
const elementHorzontal = getElement(USER_TEXT_X_AXIS)
const elementColorList = getElement(COLOR_LIST)
const elementFontList = getElement(FONT_LIST)
const elementImageList = getElement(IMAGE_LIST)
const elementImage = getElement(NEON_BACKGROUND_IMAGE)
const elementLoader = getElement(LOADER)
const elementCompletedEnquiry = getElement(COMPLETED_ENQUIRY)
const elementErrorEnquiry = getElement(ERROR_ENQUIRY)
const elementFormState = getElement(FORM_STATE)

elementInput.addEventListener('input', event => {
	elementWriting.innerHTML = event.target.value.replace(/\r\n|\r|\n/g, '<br>')
})

const formHeight = elementForm.clientHeight

const reset = () => {
	elementForm.classList.add('hidden')
	elementLoader.classList.add('hidden')
	elementCompletedEnquiry.classList.add('hidden')
	elementErrorEnquiry.classList.add('hidden')

	window.parent.postMessage(
		{
			func: 'scrollTop',
			message: formHeight * 0.75
		},
		'https://www.aislehireit.com'
	)
}

const loadingState = () => {
	reset()
	elementLoader.classList.remove('hidden')
}

const completeState = () => {
	reset()
	elementCompletedEnquiry.classList.remove('hidden')
}

const errorState = () => {
	reset()
	elementErrorEnquiry.classList.remove('hidden')

	elementFormState.innerHTML = stringMessageFromObject(formState)
}

const initialState = () => {
	reset()
	elementForm.classList.remove('hidden')
}

elementCompletedEnquiry.addEventListener('submit', event => {
	event.preventDefault()
	initialState()
})

elementErrorEnquiry.addEventListener('submit', event => {
	event.preventDefault()
	initialState()
})

elementForm.addEventListener('submit', event => {
	event.preventDefault()

	const myHeaders = new Headers()
	myHeaders.append('Content-Type', 'application/json')
	const {_replyto, ...message} = formState
	const raw = JSON.stringify({
		_replyto,
		message: stringMessageFromObject(message)
	})

	const requestOptions = {
		method: 'POST',
		headers: myHeaders,
		body: raw,
		redirect: 'follow'
	}

	loadingState()

	fetch(CONTACT_API, requestOptions)
		.then(response => {
			if (response.status !== 200) {
				throw new Error('Not a 200 response')
			}

			return response.text()
		})
		.then(result => console.log(result))
		.then(completeState)
		.catch(error => {
			console.log('error', error)
			errorState()
		})
})

elementScale.addEventListener('input', event => {
	setFontSize(
		graph(event.target.value, {min: FONT_SIZE_MIN, max: FONT_SIZE_MAX})
	)
})

elementTop.addEventListener('input', event => {
	const {height} = elementImage.getBoundingClientRect()
	const writingHeight = elementWriting.getBoundingClientRect().height
	const top = graph(event.target.value, {
		min: 0,
		max: height - writingHeight
	})

	elementWriting.style.top = `${top}px`
})

elementHorzontal.addEventListener('input', event => {
	const {width} = elementImage.getBoundingClientRect()
	const writingWidth = elementWriting.getBoundingClientRect().width
	const left = graph(event.target.value, {min: 0, max: width - writingWidth})

	elementWriting.style.left = `${left}px`
})

elementForm.addEventListener('input', event => {
	updateFormState(event.target)

	updateNeonProperties()
})

const updateNeonProperties = () => {
	Object.keys(formState).forEach(key => {
		if (key === CSS_PROPERTY_TEXT_SHADOW) {
			elementWriting.style[key] = makeShadow(formState[key])
			return
		}

		if (key === SELECTED_IMAGE) {
			elementImage.src = formState[key]
			return
		}

		elementWriting.style[key] = formState[key]
	})
}

const createColorSelection = () => {
	colorList.forEach(color => {
		elementColorList.innerHTML += `<div>
  <label class="flex-row">
    <input type="radio" name="${CSS_PROPERTY_TEXT_SHADOW}" data-value="${color.color}" value="${color.colorTitle}">
        <span class="color-sample" style="${CSS_PROPERTY_BOX_SHADOW}: 0.1rem 0.1rem 0.2rem ${color.color};"></span>${color.colorTitle}
    </label>
</div>
`

		document.querySelector(
			`input[name="${CSS_PROPERTY_TEXT_SHADOW}"]`
		).checked = true
	})
}

const createFontSelection = () => {
	fontList.forEach(fontName => {
		elementFontList.innerHTML += `<div>
    <label>
        <input type="radio" name="${CSS_PROPERTY_FONT_FAMILY}" value="${fontName}"><span class="text-size--large" style="${CSS_PROPERTY_FONT_FAMILY}: ${fontName}">${fontName.replace(
	/[-_]/g,
	' '
)}</span></label>
</div>
`

		document.querySelector(
			`input[name="${CSS_PROPERTY_FONT_FAMILY}"]`
		).checked = true
	})
}

const createImageSelection = () => {
	imageList.forEach(object => {
		elementImageList.innerHTML += `<div>
    <label>
        <input type="radio" name="${SELECTED_IMAGE}" value="${object.image}">
        ${object.name}
    </label>
</div>
`

		document.querySelector(`input[name="${SELECTED_IMAGE}"]`).checked = true
	})
}

function docReady(fn) {
	if (
		document.readyState === 'complete' ||
    document.readyState === 'interactive'
	) {
		setTimeout(fn, 1)
	} else {
		document.addEventListener('DOMContentLoaded', fn)
	}
}

const addLocation = () => {
	const object = {
		name: 'host',
		value: document.referrer
	}
	updateFormState(object)
}

docReady(() => {
	createColorSelection()
	createFontSelection()
	createImageSelection()

	elementWriting.style.top = `${elementTop.value}%`
	setFontSize(FONT_SIZE_INITIAL)

	const checkedInputs = [...document.querySelectorAll('input:checked')]
	checkedInputs.forEach(checkedInput => updateFormState(checkedInput))

	addLocation()
	setTimeout(updateNeonProperties, 10)

	window.updateNeonProperties = updateNeonProperties
})
