// IMPORTANT
//   - Imports are declared in camelCase

import darkWall from './dark-wall.jpeg'
import deskRoom from './desk-room.jpeg'
import redBrickWall from './red-brick-wall.jpeg'
import whiteWall from './white-wall.jpeg'

export default [
	{
		name: 'dark wall',
		image: darkWall
	},
	{
		name: 'desk room',
		image: deskRoom
	},
	{
		name: 'red brick wall',
		image: redBrickWall
	},
	{
		name: 'white wall',
		image: whiteWall
	}
]
