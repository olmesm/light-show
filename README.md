# Light Show

Displays a simulated Neon sign on a background, while allowing the user to change the font style, size, colour, and position.

[View it here](https://olmesm.gitlab.io/light-show/)

Note this uses <https://gitlab.com/olmesm/light-show-contact> for the contact form.

## Developing

```sh
# Optional: set the correct node version
nvm use

# Install all the dependencies
npm install

# Development server with hot reloading
npm start
```

## Adding assets

### Fonts

Please save the font file in kebab-case; eg `kebab-cased-convention.ttf`

Some font extensions are not compatible in browsers - ideally you want a [web safe font](https://www.queness.com/post/14873/19-most-useful-font-face-generators-for-converting-fonts-to-web-safe-fonts)

Fonts need to be set in the following places:

1. `src/static/fonts/font-list.json` the exact name of the font file excluding the extension
1. `src/static/fonts/` the font file location
1. `src/style.scss` the css reference

### Colours

Colours need to be set in the following places:

1. `src/static/color-list.json` where color corresponds to a [hex value](https://www.color-hex.com/)

### Background Images

Please save the image file in kebab-case; eg `kebab-cased-convention.jpeg`

Images need to be set in the following places:

1. `src/static/images/image-list.js` import the file and then declare the name and import as per structure
1. `src/static/images/` the image file location

## Deployment

Push the changes up to gitlab and the pipeline should handle the rest.

Note: `PUBLIC_URL` in the `.gitlab.yml` file sets a root path for the build.

## Embedding

Embed this in your CMS

```html
<script>
  if (window.addEventListener) {
    window.addEventListener("message", onMessage, false);
  }

  if (window.attachEvent) {
    window.attachEvent("onmessage", onMessage, false);
  }

  function onMessage(event) {
    // Check sender origin to be trusted
    if (event.origin !== "https://olmesm.gitlab.io") return;

    var data = event.data;

    if (typeof window[data.func] == "function") {
      window[data.func].call(null, data.message);
    }
  }

  function scrollTop() {
    window.scroll({
      top: window.innerHeight * 0.6,
      left: 0,
      behavior: "smooth",
    });
  }
</script>

<iframe
  style="border: none;"
  src="https://olmesm.gitlab.io/light-show"
  width="100%"
  height="2200px"
></iframe>
```
